package com.atlassian.bamboo.plugin.agentsmith.datasending;

import com.atlassian.bamboo.plugin.agentsmith.AgentSmithService;
import com.atlassian.sal.api.scheduling.PluginJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Job in charge of sending statistics on the current status of Bamboo to a graphite instance.
 */
public class StatisticUpdateJob implements PluginJob {
    /**
     * Key for the {@link DataSendingService} instance provided through the data map.
     */
    public static final String DATA_SENDING_SERVICE_KEY = "dataSendingService";
    /**
     * Key for the {@link AgentSmithService} instance provided through the data map.
     */
    public static final String AGENT_SMITH_SERVICE_KEY = "agentSmithServiceInstance";
    /**
     * Name of the job for external usage.
     */
    public static final String JOB_NAME = StatisticUpdateJob.class.getName() + ".job";
    private static final Logger logger = LoggerFactory.getLogger(StatisticUpdateJob.class);

    @Override
    public void execute(Map<String, Object> jobDataMap) {
        logger.debug("Start of the update job.");
        DataSendingService graphiteService = (DataSendingService) jobDataMap.get(DATA_SENDING_SERVICE_KEY);
        AgentSmithService agentSmithService = (AgentSmithService) jobDataMap.get(AGENT_SMITH_SERVICE_KEY);

        try {
            graphiteService.report(agentSmithService.getStatistics());
        } catch (Exception e) {
            logger.error("An exception occurred while sending the statistics to graphite: ", e);
        }

        logger.debug("End of the update job.");
    }
}
