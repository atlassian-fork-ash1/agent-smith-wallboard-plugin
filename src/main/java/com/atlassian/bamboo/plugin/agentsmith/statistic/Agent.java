package com.atlassian.bamboo.plugin.agentsmith.statistic;

/**
 * Single agent currently managed by bamboo.
 */
public class Agent {
    private final long id;
    private final Type type;
    private Status status;
    private ElasticProperties elasticProperties;
    private boolean dedicated;

    /**
     * Creates an agent.
     *
     * @param id   unique identifier of the agent.
     * @param type type of the agent.
     */
    public Agent(long id, Type type) {
        this.id = id;
        this.type = type;
        if (type == null)
            throw new IllegalArgumentException("The agent type must be provided");
        if (type == Type.ELASTIC)
            elasticProperties = new ElasticProperties();
    }

    public long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * Get additional properties for an {@link Type#ELASTIC} agent.
     *
     * @return the additional elastic properties.
     * @throws UnsupportedOperationException if the agent isn't elastic.
     */
    public ElasticProperties getElasticProperties() {
        if (type != Type.ELASTIC)
            throw new UnsupportedOperationException("Impossible to get the elastic properties of a non elastic agent.");
        return elasticProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Agent agent = (Agent) o;

        return id == agent.id && type == agent.type;
    }

    public boolean isDedicated() {
        return dedicated;
    }

    public void setDedicated(boolean dedicated) {
        this.dedicated = dedicated;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Agent '" + id + "' (" + type + ")"
                + "{Status: " + status + ", ElasticProperties: [" + elasticProperties + "]}";
    }

    /**
     * Type of agent.
     */
    public static enum Type {
        /**
         * Agent running in the same machine as the Bamboo instance.
         */
        LOCAL,
        /**
         * Remote instance of agent.
         */
        REMOTE,
        /**
         * Agent hosted on an "elastic" provider (ie. AWS).
         */
        ELASTIC
    }

    /**
     * Status of the bamboo agent.
     */
    public static enum Status {
        /**
         * Waiting for a new job to run.
         */
        IDLE,
        /**
         * Running a job.
         */
        BUSY,
        /**
         * Stuck on a job which doesn't generate new logs.
         */
        HUNG,
        /**
         * Disabled and not able to run new jobs.
         */
        DISABLED,
        /**
         * Not available anymore.
         */
        OFFLINE
    }
}
